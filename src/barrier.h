#include <cstdint>

#include "evotsc.h"

class Barrier {
 public:
  //////////// Constructors / destructors
  Barrier() = delete;
  Barrier(BarrierType type, int32_t pos, double supercoiling,
          int32_t unhook_time);

  Barrier(int32_t pos); // Make a fake barrier for the lookup

  ~Barrier() = default;

  //////////// Getters / setters

  BarrierType type() const { return type_; }
  int32_t pos() const { return pos_; }
  double supercoiling() const { return supercoiling_; }
  int32_t unhook_time() const { return unhook_time_; }

  //////////// Operators

  bool operator<(const Barrier& a) const { return pos_ < a.pos_; }

 private:
  BarrierType type_;
  int32_t pos_;
  double supercoiling_; // The DNA supercoiling from here to the next barrier
  int32_t unhook_time_;
};
