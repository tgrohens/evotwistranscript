#include <cstdio>
#include <cstdint>
#include <cmath>

#include <algorithm>
#include <vector>

#include "evotsc.h"
#include "barrier.h"
#include "transcript.h"

const int32_t max_time = 1;

const double delta_t = 2.0;

const double promoter_sigma = -0.042;
const double promoter_epsilon = 0.005;
const double promoter_m = 2.5;

int main(int argc, char** argv) {

  std::vector<Transcript> transcription_sites;
  std::vector<Barrier> barriers; // Keep this vector sorted; there is always at least one element

  //////////////////////////////////////////////////
  // Setup an example individual
  const int32_t dna_length = 1000;

  Transcript site_1(100, 200, LEADING, 0.01, promoter_sigma, promoter_epsilon,
                    promoter_m);

  Transcript site_2(300, 400, LEADING, 0.012, promoter_sigma, promoter_epsilon,
                    promoter_m);

  Transcript site_3(500, 600, LEADING, 0.008, promoter_sigma, promoter_epsilon,
                    promoter_m);

  Transcript site_4(700, 800, LEADING, 0.01, promoter_sigma, promoter_epsilon,
                    promoter_m);


  transcription_sites.push_back(site_1);
  transcription_sites.push_back(site_2);
  transcription_sites.push_back(site_3);
  transcription_sites.push_back(site_4);

  //////////////////////////////////////////////////
  // Initial state of the simulation: no barriers

  Barrier empty_barrier(EMPTY, 0, -0.06, NO_UNHOOK);

  barriers.push_back(empty_barrier);

  //////////////////////////////////////////////////
  for (int32_t time = 0; time < max_time; time++) {
    // For each transcription site, find its domain and
    // compute its initiation probability
    printf("Time: %.2d\n", time);
    for (auto& site: transcription_sites) {
      printf(" site (%d, %d)\n", site.start_pos(), site.end_pos());
      auto site_barrier = std::lower_bound(barriers.begin(), barriers.end(),
                                           Barrier(site.start_pos()));
      if (site_barrier == barriers.end()) {
        // No barrier stands before the position, ie we are in
        // the domain that starts after the last barrier
        site_barrier--;
      }
      printf(" barrier found: type %d pos %d, supercoiling %lf\n",
             site_barrier->type(), site_barrier->pos(),
             site_barrier->supercoiling());


      site.update_init_prob(site_barrier->supercoiling());
      printf("Initiation probability: SC %lf -> %.17lf\n", site_barrier->supercoiling(), site.init_prob());
    }

    double sum_init_probs = 0.0;
    for (auto& site: transcription_sites) {
      sum_init_probs += site.init_prob();
    }

    double proba_hook = 1 - exp(- (sum_init_probs * delta_t));

    for (auto& site: transcription_sites) {
      double proba_hook_site = proba_hook * site.init_prob() / sum_init_probs;
      printf("Proba to hook site %d: %.17lf\n", site.start_pos(), proba_hook_site);
    }
  }

  return 0;
}