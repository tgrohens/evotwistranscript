#include <cmath>

#include "transcript.h"

Transcript::Transcript(int32_t start_pos, int32_t end_pos, Strand strand,
                       double strength, double opt_supercoiling, double epsilon,
                       double m)
    : start_pos_(start_pos), end_pos_(end_pos), strand_(strand),
      strength_(strength), opt_supercoiling_(opt_supercoiling),
      epsilon_(epsilon), m_(m) {
    init_prob_ = 0.0;
}

void Transcript::update_init_prob(double supercoiling) {
  init_prob_ =
      strength_ *
      exp(m_ / (1 + exp((supercoiling - opt_supercoiling_) / epsilon_)));
}