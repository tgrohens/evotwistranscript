#pragma once

#include <cstdint>

enum BarrierType {
  EMPTY,
  STRUCT,
  LEADING_POL,
  LAGGING_POL
};

const int NO_UNHOOK = -1;

enum Strand {
  LEADING,
  LAGGING
};
