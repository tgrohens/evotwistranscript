#include <cstdint>

#include "evotsc.h"

class Transcript {

 public:
  Transcript() = delete;

  Transcript(int32_t start_pos, int32_t end_pos, Strand strand, double strength,
             double opt_supercoiling, double epsilon, double m);

  ~Transcript() = default;

  int32_t start_pos() const { return start_pos_; }
  int32_t end_pos() const { return end_pos_; }

  double strength() const { return strength_; }
  double opt_supercoiling() const { return opt_supercoiling_; }

  double init_prob() const { return init_prob_; }

  void update_init_prob(double supercoiling);

 private:
  // Genome position
  int32_t start_pos_;
  int32_t end_pos_;
  Strand strand_;

  // Biophysical parameters of the transcription start site
  double strength_;
  double opt_supercoiling_;
  double epsilon_;
  double m_;

  double init_prob_; // Probability that a polymerase hooks to the TSS
};
