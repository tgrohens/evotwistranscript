#include "barrier.h"

Barrier::Barrier(BarrierType type, int32_t pos, double supercoiling,
                 int32_t unhook_time)
    : type_(type), pos_(pos), supercoiling_(supercoiling),
      unhook_time_(unhook_time) {

}

Barrier::Barrier(int32_t pos) : pos_(pos) {
  type_ = EMPTY;
  supercoiling_ = 0;
  unhook_time_ = 0;
}