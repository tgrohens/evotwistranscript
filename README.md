# EvoTSC - Evolutionary Transcription-Supercoiling Coupling

This is a C++ evolution simulator, focused at studying the transcription-supercoiling coupling.

It is a (partial) reimplementation of [TwisTranscript](https://github.com/sammeyer2017/TwisTranscripT), by Meyer et al.
